(ns dn.core
  (:require [dn.read-write :as read-write]
            [dn.extract :as extract]
            [dn.transform :as transform]
            [dn.summarize :as summarize]
            [clojure.tools.cli :refer [cli]]))

(defn process-log-data [log-path]
  (->> log-path
       read-write/read-lines
       extract/extract-necessary-data
       transform/transform-data
       summarize/summarize-data
       read-write/write-result-json))

(defn -main
  [& args]
  (let [[opts args banner] (cli args
                                ["-h" "--help" "Show help" :flag true :default false]
                                ["-f" "--file" "File path" :default false])]
    (when (:help opts)
      (println banner))
    (if (:file opts)
      (do (println "Processing file" (:file opts))
          (process-log-data (:file opts)))
      (println "Please provide a log file path"))))
