(ns dn.read-write
  (:require [clojure.java.io :as io]
            [cheshire.core :as json]))

(defn read-lines [path]
  (with-open [rdr (io/reader path)]
    (reduce conj [] (line-seq rdr))))

(defn write-json [file-name data]
  (spit (str "resources/" file-name)
        (json/generate-string data {:pretty true})))

(defn write-result-json [data]
  (println "Output was saved to resources/result.json")
  (write-json "result.json" data))
