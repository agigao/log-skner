(ns dn.extract
  (:require [clojure.string :as st]))

(defn- find-timestamp [s]
  (st/trim (subs s 0 24)))

(defn- find-thread [s]
  (read-string (re-find #"\d" (re-find #"WorkerThread-\d" s))))

(defn- find-service-name [s]
  (re-find #"getRendering|startRendering" s))

(defn- find-document-id-and-page [s]
  (re-find #"\d{5,6}, \d{1}" s))

(defn- find-uid [s]
  (re-find #"\d{13}-\d{1,4}" s))

;; a length of uids
;; => {18 13082, 17 1317, 20 2112, 16 173, 15 12, 19 171}

;; {13 937}
;; {4 831, 3 92, 2 13, 1 1}


(defn- filter-raw-data
  "Extract 3 different kind of logs that we're interested in
   1. startRendering: document id and page number
   2. adding a command to queue: uid
   3. getRendering: time when rendering was finished of that very uid"
  [raw-data]
  (filter #(or (st/includes? % "Executing request startRendering")
               ;; why do we have different uids for the same document and page?
               ;; additional lines were included due to this one below, because we also have startPreviewRendering service in the log as well
               ;; (st/includes? % "Checking to add command to queue")
               (st/includes? % "Service startRendering returned")
               (st/includes? % "Executing request getRendering"))
          raw-data))

(defn- extract-vals
  "extract timestamp, service name, document-id and page, and uid"
  [data]
  (reduce #(conj %1 ((juxt find-timestamp
                           find-thread
                           find-service-name
                           find-document-id-and-page
                           find-uid) %2)) [] data))

(defn- link-with-keys
  "link data with corresponding keys"
  [data]
  (map #(zipmap [:timestamp :thread :service :document-id-and-page :uid] %) data))

(defn extract-necessary-data
  "extraction of necessary data:
    - timestamp
    - worker thread
    - service name: startRendering, uid, getRendering
    - document-id-and-page
    - uid"
  [data]
  (->> data
       filter-raw-data
       extract-vals
       link-with-keys))
