(ns dn.summarize
  (:require [dn.transform :as transform]
            [clojure.walk :as walk]))

(defn- get-doc-page-uid [v]
  (select-keys
   (first (transform/filter-service "startRendering" v))
   [:document-id :page :uid]))

(defn- count-start-get [v]
  (->> (group-by :service v)
       (walk/keywordize-keys)
       (#(merge {:start (mapv :timestamp (:startRendering %))
                 :get (mapv :timestamp (:getRendering %))}))))

(defn- analyze-renderings [data]
  (let [v (vals data)
        renderings (filter #(> (count %) 1) v)]
    (->> renderings
         (reduce #(conj %1 {:rendering (merge (get-doc-page-uid %2)
                                              (count-start-get %2))}) []))))

(defn- log-summary [renderings data]
  (let [unnecessary (filter #(= (count %) 1) (vals data))]
    {:count       (count renderings)
     :duplicates  (count (filter #(> (count (get-in % [:rendering :start])) 1) renderings))
     :unnecessary (count unnecessary)
     ;; :unnecessary {(count unnecessary) unnecessary}
     }))

(defn summarize-data [data]
  (let [renderings (analyze-renderings data)]
    (merge {:summary (log-summary renderings data)
            :report renderings})))
