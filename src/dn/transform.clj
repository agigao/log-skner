(ns dn.transform
  (:require [clojure.string :as st]))

(defn- get-uid [m1 m2]
  (if (and (some? (:uid m2))
           (= (:thread m1) (:thread m2)))
    (assoc m1 :uid (:uid m2))
    m2))

(defn filter-service [k coll]
  (filter #(= (:service %) k) coll))

(defn- assoc-uids
  " fill uid nils inside the main startRendering element and drop temporary data"
  [data]
  (->> (filter-service "startRendering" data)
       ;; associate corresponding values to :uid key
       (reduce (fn [coll x] (conj coll (get-uid (last coll) x))) [])
       ;; drop maps w/uid nils
       (filter #(some? (:uid %)))
       (concat (filter-service "getRendering" data))))


(defn- process-doc-id-page [m]
  (if-let [doc-page (:document-id-and-page m)]
    (->> doc-page
         (#(st/split % #","))
         (map (comp read-string st/trim))
         (zipmap [:document-id :page])
         (merge m)
         (#(dissoc % :document-id-and-page)))
    m))

(defn- separate-doc-id-and-page [data]
  (reduce (fn [coll x] (conj coll (process-doc-id-page x))) [] data))

(defn- discard-singular-get-renderings [data]
  (filter (fn [[k vs]] (not (and (= (count vs) 1)
                                 (= (:service (first vs)) "getRendering")))) data))

(defn- group-and-discard
  " group data over the uid and discard some getRendering elements"
  [data]
  (->> data
       (group-by :uid)
       ;; reduces number of records from  1122 to 874
       discard-singular-get-renderings))

(defn transform-data
  "Transform extracted data: assign uid to startRendering service and filter out data"
  [data]
  (->> data
       assoc-uids
       separate-doc-id-and-page
       group-and-discard))
